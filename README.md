### QA Engineer Case Study
- [ ] QA Testing Plan
    - [ ] Acceptance Criteria
    - [ ] Manual Testing Plan
- [ ] Implement Testing Framework
- [ ] Build 1 Automated Test into build process
    - [ ] Automated Test ensuring valid response from DarkSky API

### Front-End Engineer Case Study
- [x] Node & Express 
- [x] Twig
- [x] Styling must be written in Sass/SCSS, and use a compiler such as Gulp to pipe to CSS.
- [x] ES6 syntax in development.
- [x] API key must not be exposed on the page
- [x] User should be able to enter a zip code and get weather results.
- [x] Site must be mobile responsive.
- [x] Javascript on the page should follow cross-browser compatibility best practices
- [x] Bonus
    - [x] Use an IP Lookup or similar to initially load the weather of the visitor's location.
    - [x] Use React or Vue components on the page.
    - [x] The forecast should have personalized icons based on the result (Sunny day shows sunny icon for example).
    - [x] How can the page be best optimized for SEO from a front-end perspective?
      - Correct meta tag structure
      - OpenGraph tags
      - Semantic structure of HTML elements
      - Optimize all imagery
      - Enable GZip on server if applicable
      - Must be responsive for Mobile First Indexing
      - Use Schema.org structured data where applicable
      - Ensure everything adheres to minimum WCAG 2.0 guidelines

## Project Setup
- [x] Install Express
- [x] Install Webpack
- [x] Install Babel
- [x] Install Gulp
- [x] Install Autoprefixer
- [x] Install ESLint
- [x] Install polyfills for IE (promise, fetch)

## To-do
- [x] Favicon
- [x] Weather SVG icons
- [x] Geolocation
- [x] Darksky API fetch
- [x] Zip Code Input
- [x] Header
- [x] Footer
- [x] Forecast Cards

### Weather Icons / Background Images
- [x] clear-day
- [x] clear-night
- [x] rain
- [x] snow
- [x] sleet
- [x] wind
- [x] fog
- [x] cloudy
- [x] partly-cloudy-day
- [x] partly-cloudy-night

### Enhancements to be made
 - [x] Clean up design
 - [x] Masking ZIP code input
 - [x] Error Handling
   - [x] On no geolocation available
   - [x] On error from Google API
   - [x] On error from Darksky API
   - [x] On invalid zipcode (eg. 99999,00000, etc)
 - SVG Animations

 
 