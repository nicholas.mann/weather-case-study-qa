require('dotenv').config()
const express = require('express')
const { createProxyMiddleware } = require('http-proxy-middleware')
const path = require('path')
const app = express()
const port = process.env.PORT || 5000
const DIST_DIR = path.join(__dirname, '../dist')
const HTML_FILE = path.join(DIST_DIR, 'index.html')
const darkskyKey = process.env.DARKSKY_KEY

app.use(express.static(DIST_DIR))
app.use('/proxy', createProxyMiddleware({
  pathRewrite: {
     '^/proxy/': '/'
  },
  target: `https://api.darksky.net/forecast/${darkskyKey}`,
  secure: false,
  headers: {
    host: 'api.darksky.net'
  }
}));

app.get('*', (req, res) => {
  res.sendFile(HTML_FILE)
})

app.listen(port, () => {
  console.log('Listening on port: ', port)
})