import 'whatwg-fetch'
import VMasker from 'vanilla-masker'
import React from 'react'
import ReactDOM from 'react-dom'
import Search from './components/search/Search'
import Headline from './components/headline/Headline'
import Forecast from './components/forecast/Forecast'
import Currently from './components/currently/Currently'
import Background from './components/background/Background'
const googleKey = 'AIzaSyADjsU8IFHm6xy3SVZaPlRxl_xJnpUIdFA'

document.documentElement.classList.remove('no-js')

export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      zip: '',
      lat: '',
      lng: '',
      currently: [],
      forecast: [],
      loaded: false,
      headline: 'Loading weather...',
      hasError: false
    }
    this.getInitialLocation = this.getInitialLocation.bind(this)
    this.getLocationByZip = this.getLocationByZip.bind(this)
    this.getZipFromCoords = this.getZipFromCoords.bind(this)
    this.getForecast = this.getForecast.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    this.getInitialLocation()
      .then((position) => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        })
      })
      .then(() => {
        this.getZipFromCoords()
      })
      .catch((err) => {
        let errMsg
        if (err.code == 1) {
          errMsg = 'Access to location was denied. Please search by ZIP.' 
        } else if (err.code == 3) {
          errMsg = 'Sorry it\'s taking awhile to locate you. Try entering a ZIP code instead'
        } else {
          errMsg = 'Darn, we were unable to locate you &#128064;<br/>Search by ZIP code instead.'
        }
        this.setState({
          headline: errMsg
        })
      })
    const zipInput = document.querySelector('#zip')
    VMasker(zipInput).maskPattern('99999')
  }

  getInitialLocation() {
    const positionOptions = {
      timeout: 30 * 1000,
      maximumAge: 60 * 60000
    }
    return new Promise(function(resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject, positionOptions)
    })
  }

  getZipFromCoords() {
    fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.state.lat},${this.state.lng}&key=${googleKey}`)
      .then((response) => response.json())
      .then((response) => {
        let components = response.results[0].address_components
        let results = components.filter((el) => {
          return el.types == 'postal_code'
        })
        this.setState({
          zip: results[0].long_name
        })
      })
      .then(() => {
        this.getLocationByZip(this.state.zip, this.getForecast)
      })
      .catch((err) => {
        console.log(err)
        this.setState({
          hasError: true,
          headline: 'You seem to be outside the U.S. &#9992; ... Right now we only work inside the United States.'
        })
      })
  }

  getLocationByZip(zip, callback) {
    if (zip.length < 5) {
      this.setState({
        hasError: true,
        headline: 'Oops, that doesn\'t seem to be a valid ZIP Code'
      })
      return false
    }
    fetch(`https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:${zip}|country:US&key=${googleKey}`)
      .then((response) => response.json())
      .then((response) => {
        if (response.results.length < 1) {
          throw new Error('No Results for that ZIP code')
        }
        this.setState({
          zip: response.results[0].address_components[0].short_name,
          lat: response.results[0].geometry.location.lat,
          lng: response.results[0].geometry.location.lng,
          headline: 'Current Weather for ' + response.results[0].formatted_address,
          hasError: false
        })
      })
      .then(() => callback())
      .catch((err) => {
        console.log(err)
        this.setState({
          hasError: true,
          headline: 'Sorry, we\'re unable to locate you based on that ZIP code. Please check that it is valid and try again.'
        })
      })
  }

  getForecast() {
    fetch(`/proxy/${this.state.lat},${this.state.lng}`)
      .then((response) => response.json())
      .then((response) => {
        this.setState({
          loaded: true,
          currently: response.currently,
          forecast: response.daily.data.slice(0, 7)
        })
      })
      .catch((err) => {
        console.log(err)
        this.setState({
          hasError: true,
          headline: 'Terribly sorry, our weatherperson must have fallen alseep, please try again shortly.'
        })
      })
  }

  handleChange(event) {
    this.setState({
      zip: event.target.value
    })
  }

  handleSubmit(event) {
    const zipInput = event.target.querySelector('#zip')
    event.preventDefault()
    this.setState({
      zip: zipInput.value,
    })
    this.getLocationByZip(this.state.zip, this.getForecast)
    zipInput.value = ''
  }

  render() {
    return (
      <main className={`main${this.state.loaded ? ' app-loaded' : ''}${this.state.hasError ? ' app-failed' : ''}`}>
        <Background loaded={this.state.loaded} error={this.state.error} currentWeather={this.state.currently.icon}/>
        <Search handleChange={this.handleChange} handleSubmit={this.handleSubmit}/>
        <Headline headline={this.state.headline}/>
        <Currently loaded={this.state.loaded} currently={this.state.currently} error={this.state.hasError}/>
        <Forecast loaded={this.state.loaded} forecast={this.state.forecast} error={this.state.hasError}/>
      </main>
    )
  }
}

ReactDOM.render(<App />, document.querySelector('#app'))