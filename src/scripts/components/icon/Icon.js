import React from 'react'
import IconClearDay from '../icons/IconClearDay'
import IconClearNight from '../icons/IconClearNight'
import IconCloudy from '../icons/IconCloudy'
import IconFog from '../icons/IconFog'
import IconPartlyCloudyNight from '../icons/IconPartlyCloudyNight'
import IconPartlyCloudyDay from '../icons/IconPartlyCloudyDay'
import IconRain from '../icons/IconRain'
import IconSleet from '../icons/IconSleet'
import IconSnow from '../icons/IconSnow'
import IconWind from '../icons/IconWind'

const Icon = ({ container, icon }) => {
  if (icon =='clear-day') {
    icon = <IconClearDay/>
  } else if (icon == 'clear-night') {
    icon = <IconClearNight/>
  } else if (icon == 'cloudy') {
    icon = <IconCloudy/>
  } else if (icon == 'fog') {
    icon = <IconFog/>
  } else if (icon == 'partly-cloudy-night') {
    icon = <IconPartlyCloudyNight/>
  } else if (icon == 'partly-cloudy-day') {
    icon = <IconPartlyCloudyDay/>
  } else if (icon == 'rain') {
    icon = <IconRain/>
  } else if (icon == 'sleet') {
    icon = <IconSleet/>
  } else if (icon == 'snow') {
    icon = <IconSnow/>
  } else if (icon == 'wind') {
    icon = <IconWind/>
  }

  return (
    <div className={container}>
      { icon }
    </div>
  )

}

export default Icon