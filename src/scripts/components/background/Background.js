import React from 'react'

const Background = ({ loaded, error, currentWeather }) => {
  let image
  let alt

  if (loaded && !error) {
    image = `/images/${currentWeather}.jpg`
    alt = `An image representing ${currentWeather}`
  }

  return (
    <div className="background">
      <img className="loader" src="/images/loader.svg" alt=""/>
      <img src={image} alt={alt}/>
    </div>
  )

}

export default Background