import React from 'react'

const Search = ({ handleChange, handleSubmit }) => (
  <section className="search-container">
    <h2>Enter a ZIP code to get the weather!</h2>
    <form className="zip-search" onSubmit={(event) => (handleSubmit(event))}>
      <label className="visually-hidden" htmlFor="zip">Enter a 5 digit ZIP Code to search</label>
      <input type="text" id="zip" className="zip-input" pattern="[0-9]{5,}" placeholder="Enter a 5 digit ZIP" onChange={handleChange} title="ZIP Code must contain only numbers and be 5 digits long." required />
      <input className="button button--primary" type="submit" value="Search"/>
    </form>
  </section>
)

export default Search