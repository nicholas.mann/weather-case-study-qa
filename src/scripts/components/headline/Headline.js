import React from 'react'

const Headline = ({ headline }) => (
  <section className="location-headline">
    <h2 dangerouslySetInnerHTML={{ __html: headline }}></h2>
  </section>
)

export default Headline