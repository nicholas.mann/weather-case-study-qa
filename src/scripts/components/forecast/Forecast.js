import React from 'react'
import Daily from '../daily/Daily'

const Forecast = ({ forecast, loaded, error }) => {
  let sectionHeadline = ''
  let weeklyForecastContainer

  if (!error && loaded) {
    sectionHeadline = <h2>Weekly Forecast</h2>

    weeklyForecastContainer =
      <div className="weekly-forecast__inner">
        {forecast.map((day, index) => 
          <Daily key={index}
            date={day.time}
            icon={day.icon}
            summary={day.summary}
            temperatureLow={day.temperatureLow}
            temperatureHigh={day.temperatureHigh}
            precipProbability={day.precipProbability}
            uvIndex={day.uvIndex}
            windSpeed={day.windSpeed}
            windGust={day.windGust}
          />
        )}
      </div>
  } else {
    weeklyForecastContainer = ''
  }

  return (
    <section className="weekly-forecast">
      {sectionHeadline}
      {weeklyForecastContainer}
    </section>
  )
}

export default Forecast