import React from 'react'
import Icon from '../icon/Icon'

const Currently = ({ error, loaded, currently }) => {
  let currentlyContainer = ''

  if (!error && loaded) {
    let stormDistance = currently.nearestStormDistance
    let stormDirection = currently.nearestStormBearing
    let stormMessage = 'No storms in sight &#128526;'

    switch (true) {
    case (stormDirection < 45):
      stormDirection = 'North'
      break
    case (stormDirection < 90):
      stormDirection = 'Northeast'
      break
    case (stormDirection == 90):
      stormDirection = 'East'
      break
    case (stormDirection < 135):
      stormDirection = 'Southeast'
      break
    case (stormDirection == 180):
      stormDirection = 'South'
      break
    case (stormDirection < 225):
      stormDirection = 'Southwest'
      break
    case (stormDirection == 270):
      stormDirection = 'West'
      break
    case (stormDirection < 315):
      stormDirection = 'Northwest'
      break
    case (stormDirection < 360):
      stormDirection = 'North'
      break
    default:
      stormDirection = null
      break
    }

    if (stormDistance > 100) {
      stormMessage = 'No storms nearby.'
    } else if (stormDistance == 0) {
      stormMessage = 'There is a storm in your vicinity.'
    } else {
      stormMessage = `There is a storm <strong>${stormDistance}</strong> miles away to the <strong>${stormDirection}<strong>`
    }

    currentlyContainer =
      <div className="currently__inner">
        <Icon container="currently__icon" icon={currently.icon}/>
        <div className="currently__temp">
          <span>{Math.round(currently.temperature)}&deg;</span>
        </div>
        <div className="currently__summary">
          <span>{currently.summary}</span>
          <span dangerouslySetInnerHTML={{ __html: stormMessage }}></span>
        </div>
        <div className="currently__data">
          <span>Precip Chance: <strong>{currently.precipProbability}%</strong></span>
          <span>Cloud Cover: <strong>{currently.cloudCover * 100}%</strong></span>
          <span>Wind Speed: <strong>{currently.windSpeed}MPH</strong></span>
        </div>
      </div>
  }

  return (
    <section className="currently">
      {currentlyContainer}
    </section>
  )
}

export default Currently