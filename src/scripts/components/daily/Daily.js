import React from 'react'
import Icon from '../icon/Icon'

const Daily = ({ date, icon, summary, temperatureHigh, temperatureLow, precipProbability, uvIndex, windSpeed, windGust }) => {
  let formattedDate = new Date(date * 1000).toLocaleString('en-US', {
    weekday: 'short'
  })

  return (
    <div className="daily">
      <p className="daily__day">{formattedDate}</p>
      <Icon container="daily__icon" icon={icon}/>
      <div className="daily__temps">
        <span className="low">Low: <span className="temp">{Math.round(temperatureLow)}&deg;</span></span><span className="high">High: <span className="temp">{Math.round(temperatureHigh)}&deg;</span></span>
      </div>
      <p className="daily__summary">{summary}</p>
      <div className="daily__data">
        <p>Precip Chance: <strong>{Math.round(precipProbability * 100) + '%'}</strong></p>
        <p>UV Index: <strong>{uvIndex}</strong></p>
        <p>Wind: <strong>{Math.round(windSpeed)}MPH</strong></p>
        <p>Gust: <strong>{Math.round(windGust)}MPH</strong></p>
      </div>
    </div>
  )
}

export default Daily