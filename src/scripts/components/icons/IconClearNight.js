import React from 'react'

const IconClearNight = () => {
  return(
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41.6 41.7" overflow="visible">
      <path d="M39.3 23.9c-11.5 0-24.2-9.1-18.4-23.9C8.5-.1 0 10 0 20.8c0 11.5 9.3 20.9 20.9 20.9 10.5 0 19.2-7.6 20.7-17.7 0-.1-1.5-.1-2.3-.1zM21.2 38.6c-9.9 0-17.9-8-17.9-17.9 0-8.2 5.8-15.1 13.3-17.3v.1c-1.8 12.5 7.1 23 20.7 24.2-3.5 6.8-8.3 10.6-16.1 10.9z" fill="#72b8d4"/>
    </svg>
  )
}

export default IconClearNight